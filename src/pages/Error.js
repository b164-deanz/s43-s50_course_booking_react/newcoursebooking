	import {Container} from 'react-bootstrap'
import logo from './Error.png'

export default function ErrorPage(){

	return(

		<Container className="text-center">
		<img src={logo} alt="logo" />
		<h1 className="text-center ">404 Page Found</h1>
		</Container>

		)


}