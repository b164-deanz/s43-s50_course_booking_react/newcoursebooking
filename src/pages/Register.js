import {Form, Button} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import UserContext from '../UserContext'
import {Navigate, useNavigate} from 'react-router-dom'
import Swal from 'sweetalert2'


export default function Register (){

	const {user} = useContext(UserContext)

	const navigate = useNavigate()

	const [firstName, setFirstName] = useState("")
	const [lastName, setLastName] = useState("")
	//State hooks to store the valuse of the input fields.
	const [email, setEmail] = useState("")
	const [mobileNo, setMobileNo] = useState("")
	const[Password1, setPassword1] = useState("");
	const[Password2, setPassword2] = useState("");
	
	//Stae to determining whether the button is enabled or not
	const[isActive, setIsActive] = useState(false);


	function registerUser(e){

		//prevent page redirection via form submission
		e.preventDefault()

		fetch("http://localhost:4000/api/users/checkEmail", {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true){
				Swal.fire({
					title: "Duplicate email found",
					icon: "error",
					text: "Kindly provide another email to complete the registration"
				})
			} else{
				fetch("http://localhost:4000/api/users/register", {
					method: "POST",
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: Password1
					})
				})
				.then(res => res.json())
				.then(data => {
					if(data === true){
						setFirstName("")
						setLastName("")
						setEmail("");
						setMobileNo("")
						setPassword1("");
						setPassword2("");

						Swal.fire({
							text: 'Registration Successfull',
							icon: 'success',
							text: 'Welcome to Batch 164, Course Booking'
						})
						 .then((result) => {
                 		 navigate('/login')
               			 })
					} else{
						Swal.fire({
							title: 'Something went wrong',
							icon: "error",
							text: 'Please try again.'
						})
					}
				})
			}
		})

		//clear input fields
		
		// localStorage.setItem("email", email)

		// alert("Thank You for Registering")
	}

	useEffect(() => {
		//Validation or enable the submit button when all the input fields are populated and both passwords match
		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo.length === 11 && Password1 !== "" && Password2 !== "") && (Password1 === Password2)){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [firstName, lastName, mobileNo, email, Password1, Password2])


	return(
		(user.id !== null) ?
		<Navigate to="/courses" />
		:
		<Form onSubmit={(e) => registerUser(e)}>
		<h1 className="text-center">Register</h1>
		<Form.Group 
		className="mb-3" 
		controlId="firstName">

		  <Form.Label>First Name:</Form.Label>
		  <Form.Control 
		  type="text" 
		  placeholder="Enter First Name"
		  value={firstName}
		  onChange={e => setFirstName(e.target.value)}

		  required />
		</Form.Group>

		<Form.Group 
		className="mb-3" 
		controlId="lastName">

		  <Form.Label>Last Name</Form.Label>
		  <Form.Control 
		  type="text" 
		  placeholder="Enter Last Name"
		  value={lastName}
		  onChange={e => setLastName(e.target.value)}

		  required />
		</Form.Group>

		  <Form.Group 
		  	className="mb-3" 
		  	controlId="userEmail"

		  >
		    <Form.Label>Email address</Form.Label>
		    <Form.Control 
		    type="email" 
		    value={email}
		    onChange={e =>{ 
		    	setEmail(e.target.value)
		    	// console.log(e.target.value)
		    }}
		    placeholder="Enter email"
		    required />
		    <Form.Text className="text-muted">
		      We'll never share your email with anyone else.
		    </Form.Text>
		  </Form.Group>

		  <Form.Group 
		  className="mb-3" 
		  controlId="moileNo">

		    <Form.Label>Mobile Number</Form.Label>
		    <Form.Control 
		    type="text" 
		    placeholder="Enter Mobile Number"
		    value={mobileNo}
		    onChange={e => setMobileNo(e.target.value)}

		    required />
		  </Form.Group>

		  <Form.Group 
		  className="mb-3" 
		  controlId="password1">

		    <Form.Label>Password</Form.Label>
		    <Form.Control 
		    type="password" 
		    placeholder="Password"
		    value={Password1}
		    onChange={e => setPassword1(e.target.value)}

		    required />
		  </Form.Group>

		   <Form.Group 
		   className="mb-3" 
		   controlId="password2">
		    <Form.Label>Verify Password</Form.Label>
		    <Form.Control 
		    type="password" 
		    placeholder="Password"
		    value={Password2}
		    onChange={e => setPassword2(e.target.value)}
		    required
		     />
		  </Form.Group>


		{/**/}
		{
			isActive ?
			  <Button 
		  variant="primary" 
		  type="submit"
		  id="submitBtn"
		  >
		    Submit
		  </Button>
		  :
		    <Button 
		  variant="danger" 
		  type="submit"
		  id="submitBtn"
		  disabled
		  >
		    Submit
		  </Button>
		}

		</Form>


		);
};