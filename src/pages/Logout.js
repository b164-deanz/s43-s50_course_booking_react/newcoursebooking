import {Navigate} from 'react-router-dom';
//Redirect

import {useContext, useEffect} from 'react'
import UserContext from '../UserContext'

export default function Logout(){
	
	const {unsetUser, setUser} = useContext(UserContext)
	unsetUser()

	//By adding the use effect, this will allow the logout page to render first before triggering the useEffect which changes the state of our user.
	useEffect(() => {
		setUser({id: null})
	}, [])

	return(
		
		<Navigate to="/" />

	);
}