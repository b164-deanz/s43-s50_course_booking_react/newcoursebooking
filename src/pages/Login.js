import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import {Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Login(){

	//"useContext" hook is used to deconstruct/unpack the data of the userContext object
	const {user, setUser} = useContext(UserContext)


	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")

	const[isActive, setIsActive] = useState(false);


	function loginUser(e){
		e.preventDefault()

		/*
		Syntax:
			fetch("URL", {options})
			.then(res => res.json)
			.then(data => {})
		*/

		fetch("http://localhost:4000/api/users/login", {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)

			if(typeof data.accessToken !== "undefined"){
				localStorage.setItem('token', data.accessToken)
				retrieveUserDetails(data.accessToken)

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to Zuitt"
				})
			}else{
				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your login details and try again"
				})
			}
		})

		setEmail("");
		setPassword("")

		/*
			Syntax:
				localStorage.setItem("propertyName", value)
		*/

		// localStorage.setItem("email", email)

		//Set the global user state to have properties from local storage

		// setUser({
		// 	email: localStorage.getItem('email')
		// })

		// alert(`${email}You are not Logged in`)
	}

	const retrieveUserDetails = (token) => {
		fetch('http://localhost:4000/api/users/details', {
			headers: {
				Authorization: `Bearer ${ token }`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	useEffect(() => {
		if(email !== "" && password !== ""){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [email, password])


	return(
		//Redirecting
		(user.id !== null) ?
		<Navigate to="/courses" />
		:

		<Form onSubmit={(e) => loginUser(e)}>
		<h1 className="text-center">Login Page</h1>
		  <Form.Group 
		  	className="mb-3" 
		  	controlId="email"

		  >
		    <Form.Label>Email address</Form.Label>
		    <Form.Control 
		    type="email" 
		    value={email}
		    onChange={e =>{ 
		    	setEmail(e.target.value)
		    	// console.log(e.target.value)
		    }}
		    placeholder="Enter email"
		    required />
		    <Form.Text className="text-muted">
		      We'll never share your email with anyone else.
		    </Form.Text>
		  </Form.Group>

		  <Form.Group 
		  className="mb-3" 
		  controlId="password">

		    <Form.Label>Password</Form.Label>
		    <Form.Control 
		    type="password" 
		    placeholder="Password"
		    value={password}
		    onChange={e => setPassword(e.target.value)}

		    required />
		  </Form.Group>


		{/**/}
		{
			isActive ?
			  <Button 
		  variant="primary" 
		  type="submit"
		  id="submitBtn"
		  >
		    Submit
		  </Button>
		  :
		    <Button 
		  variant="danger" 
		  type="submit"
		  id="submitBtn"
		  disabled
		  >
		    Submit
		  </Button>
		}

		</Form>

		)


}